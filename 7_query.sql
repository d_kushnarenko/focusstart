/*
Есть таблица источник GRATH_SRC и TEMP_GRATH
В результат запроса должны попасть новые записи, удаленные и измененные только по полю weight_src
*/
with GRATH_SRC as 
(
select 1 as id, 100 as from_src, 102 as to_src, 100 as weight_src from dual union all
select 2,100, 101, 400 from dual union all
select 3,101, 102, 200 from dual union all
select 4,300, 301, 300 from dual union all
select 5,300, 302, 400 from dual union all
select 6,301, 302, 500 from dual
),
TEMP_GRATH as (
select 100 as from_temp, 101 as to_temp, 200 as weight from dual union all
select 200, 201, 200 from dual union all
select 300, 301, 300 from dual union all
select 300, 302, 400 from dual union all
select 301, 302, 600 from dual
)

select *
  from GRATH_SRC gs
  full outer join TEMP_GRATH tg
    on gs.from_src = tg.from_temp
   and gs.to_src = tg.to_temp
 where tg.from_temp is null
    or gs.from_src is null
    or tg.weight <> gs.weight_src