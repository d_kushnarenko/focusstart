/*
�������� ��������� ������������������:
    100
    101
    102
    103
*/

with grath_src as 
(
select 1 as id, 100 as from_src, 101 as to_src from dual union all
select 2,100, 102 from dual union all
select 3,100, 103 from dual union all
select 4,101, 102 from dual union all
select 5,101, 103 from dual union all
select 6,102, 103 from dual
)
select from_src from grath_src
union
select to_src from grath_src
order by 1
