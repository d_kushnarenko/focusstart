/*
2. ≈сть столбец, содержащий следующие значени¤:
  100
   96
   500
   987
   799
   80
   300
   657
»сключите значени¤, которые передаютс¤ строкой в запрос -  Т96,300,799Т
*/
--¬ариант є1 (Ѕез ввода с клавиатуры)
with tab as 
(
select 100  as value from dual union all select 96 from dual union all
select 500 from dual union all select 987 from dual union all
select 799 from dual union all select 80 from dual union all
select 300 from dual union all select 657 from dual
)
select *
  from tab
WHERE instr ('96,300,799', to_char(value)) = 0
--согласен, более красивый вариант. мне он больше нравится--
