/*6. ������� �������, ID ������ � �������� ���� �����������, 
�������� ������� ����� ����������� �������� ������-���� ������*/

select emp.last_name,
       emp.job_id,
       emp.salary
  from employees emp
 where emp.salary in (
                       select distinct min(salary) 
                              over(partition by department_id) 
                       from employees
                      )
