/*
3.������ N-���� ������� � ������. ��������� ������� � ������� substr  � regexp_replace
208010100000 -> 2080X0100000
208010100000 -> 2X8010100000
*/

--SUBSTR--
select substr('208010100000',1,4) || 'X' || substr('208010100000',6,12) as output_line from dual
union all
select substr('208010100000',1,1) || 'X' || substr('208010100000',3,12) from dual;


--REGEXP_REPLACE--
select regexp_replace('208010100000','.','X', 5, 1) as output_line from dual
union all
select regexp_replace('208010100000','.','X', 2, 1) from dual;
